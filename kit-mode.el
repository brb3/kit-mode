;;; kit-mode.el --- Major mode for editing Kit Lang files. -*- coding: utf-8; lexical-binding: t; -*-

;; Copyright © 2018, by Bobby Burden III

;; Author: Bobby Burden III <bobby@brb3.org>
;; Version: 1.0.0
;; Created: 20 September 2018
;; Keywords: languages
;; Homepage:

;; This file is not part of GNU Emacs.

;;; License:

;; You can redistribute this program and/or modify it under the terms of the GNU
;; General Public License version 2.

;;; Commentary:

;; Major mode for editing Kit Lang files

;; Currently, this mode only provides syntax highlighting for Kit Lang files.
;; This is based on the Major Mode example by Xah Lee from
;; http://ergoemacs.org/emacs/elisp_syntax_coloring.html

;;; Code:

;; create the list for font-lock.
;; each category of keyword is given a particular face

;;; Customize variables
(defgroup kit nil
  "A major mode for the Kit programming language."
  :group 'languages)

(defcustom kit-tab-width 4
  "The tab width to use when indenting Kit."
  :type 'integer)

(defvar kit-font-lock-keywords
      (let* (
	    ;; define several category of keywords
	    (kit-keywords
	     '("abstract" "box" "defer" "enum" "for" "function" "if" "import"
	       "match" "public" "sizeof" "struct" "trait" "typdef" "union"
	       "using" "var" "while"))
	    (kit-types '("Box" "CString" "Float" "Int" "Ptr"))
	    (kit-constants '("Self" "this"))
	    (kit-events '())
	    (kit-functions '("main" "printf"))

	    ;; generate regex string for each category of keywords
	    (kit-keywords-regexp (regexp-opt kit-keywords 'words))
	    (kit-types-regexp (regexp-opt kit-types 'words))
	    (kit-constants-regexp (regexp-opt kit-constants 'words))
	    (kit-events-regexp (regexp-opt kit-events 'words))
	    (kit-functions-regexp (regexp-opt kit-functions 'words)))

	`(
	  (,kit-types-regexp . font-lock-type-face)
	  (,kit-constants-regexp . font-lock-constant-face)
	  (,kit-events-regexp . font-lock-builtin-face)
	  (,kit-functions-regexp . font-lock-function-name-face)
	  (,kit-keywords-regexp . font-lock-keyword-face)
	  )))

;;;###autoload
(define-derived-mode kit-mode c-mode "kit mode"
  "Major mode for editing Kit Lang files."
  (make-local-variable 'kit-tab-width)
  (setq-local font-lock-defaults '((kit-font-lock-keywords)))
  (setq-local tab-width kit-tab-width))

(provide 'kit-mode)

;;; kit-mode.el ends here

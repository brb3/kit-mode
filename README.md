# kit-mode

## Installation
Download the `kit-mode.el` file. Then add this to your `.emacs` file.

```lisp
(add-to-list 'load-path "/path/to/kit-mode/")
(autoload 'kit-mode "kit-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.kit\\'" . kit-mode))
```
